import pygame
from config import *
import sys
import json
import random
import math
pygame.init()
screen = pygame.display.set_mode(WINDOW_SIZE)
pygame.display.set_caption("SolarSystem")
clock = pygame.time.Clock()



class Planet():
    def __init__(self, name, coords, color, distance, radius, speed):
        self.name = name
        self.coords = coords
        self.color = color
        self.distance = distance
        self.radius = radius
        self.speed = speed
        self.rot = 0
        self.Surface = pygame.Surface((self.radius*2+self.distance*2,self.radius*2+self.distance*2), pygame.SRCALPHA)#
        self.orbit = 0
        self.cord = (self.Surface.get_size()[0]//2+(math.cos(self.rot)*self.distance),self.Surface.get_size()[1]//2+(math.sin(self.rot)*self.distance))
        pygame.draw.circle(self.Surface, self.color, self.cord, self.radius)
    def rotate(self):
        self.rot -= self.speed
        self.Surface = pygame.Surface((self.radius*2+self.distance*2,self.radius*2+self.distance*2), pygame.SRCALPHA)#
        pygame.draw.circle(self.Surface, (255,255,255), (self.Surface.get_size()[0]//2,self.Surface.get_size()[1]//2), self.distance, width=1)
        self.cord = (self.Surface.get_size()[0]//2+(math.cos(self.rot)*self.distance),self.Surface.get_size()[1]//2+(math.sin(self.rot)*self.distance))
        pygame.draw.circle(self.Surface, self.color, self.cord , self.radius)
    def draw(self):
        screen.blit(self.Surface,(self.coords[0]-self.Surface.get_size()[0]//2,self.coords[1]-self.Surface.get_size()[1]//2))
    def get_planet_center(self):
        return (self.coords[0]-self.Surface.get_size()[0]//2+self.cord[0],self.coords[1]-self.Surface.get_size()[1]//2+self.cord[1])
    def set_center(self,center):
        self.coords = center

scale = 0.5
fps = 120
speed = 20
au = 1
Sun = Planet("Sun", (WITHD//2,HEIGHT//2), (255, 225, 0), 0, 80*scale, 0) 
Mercury = Planet("Mercury", Sun.get_planet_center(), (176, 134, 19), 60, 10*scale, 0.68/fps*speed) 
Venus = Planet("Venus", Sun.get_planet_center(), (186, 174, 141), 90, 18*scale, 0.026/fps*speed) 
Earth = Planet("Earth", Sun.get_planet_center(), (0, 229, 255), 150, 23*scale, 0.04/fps*speed) 
Moon = Planet("Moon",Earth.get_planet_center(), (138, 138, 138), 30, 5, 0.46/fps*speed)
Mars = Planet("Mars", Sun.get_planet_center(), (176, 92, 19), 200, 15*scale, 0.008746/fps*speed) 
Jupiter = Planet("Jupiter", Sun.get_planet_center(), (158, 140, 93), 300, 60*scale, 0.001386/fps*speed)
Saturn = Planet("Saturn", Sun.get_planet_center(), (133, 116, 72), 400, 50*scale, 0.0005587/fps*speed) 
Uranus = Planet("Uranus", Sun.get_planet_center(), (36, 117, 108), 450, 40*scale, 0.00042531/fps*speed) 
Neptune = Planet("Neptune", Sun.get_planet_center(), (14, 92, 181), 500, 30*scale, 0.0001/fps*speed) 
start =  [WITHD//2,HEIGHT//2]

while True:
    screen.fill(backGroundColor) #set the background      
    for event in pygame.event.get():
        if event.type == pygame.QUIT: sys.exit()

    start = pygame.mouse.get_pos()
    Sun.draw()
    Mercury.draw()
    Venus.draw()
    Earth.draw()
    Moon.draw()
    Mars.draw()
    Jupiter.draw()
    Saturn.draw()
    Uranus.draw()
    Neptune.draw()
    Sun.set_center(start)
    Mercury.rotate()
    Mercury.set_center(Sun.get_planet_center())
    Venus.rotate()
    Venus.set_center(Sun.get_planet_center())
    Earth.rotate()
    Earth.set_center(Sun.get_planet_center())
    Moon.set_center(Earth.get_planet_center())
    Moon.rotate()
    Mars.set_center(Sun.get_planet_center())
    Mars.rotate()
    Jupiter.rotate()
    Jupiter.set_center(Sun.get_planet_center())
    Saturn.rotate()
    Saturn.set_center(Sun.get_planet_center())
    Uranus.rotate()
    Uranus.set_center(Sun.get_planet_center())
    Neptune.rotate()
    Neptune.set_center(Sun.get_planet_center())

    

   

    pygame.display.flip() # display update
    clock.tick(fps) #set fps to 60